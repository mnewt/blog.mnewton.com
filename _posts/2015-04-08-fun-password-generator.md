---
title: Fun Password Generator
date: 2015-04-08 12:14
layout: post
---

Here is a simple random password generator. It is intended to be used with a password manager, since no attempt has been made to make the passwords memorable by humans.

## Link

[`https://gist.github.com/mnewt/8d2eef4150d93a90d273`](https://gist.github.com/mnewt/8d2eef4150d93a90d273)

## Why

I made this because I couldn't find a tool that fit all of my criteria:

* portable (run unmodified on OS X, Linux, and Windows)
* all options being equal, generates completely random passwords from the entire given set of characters
* uses a complete (semi) ASCII character set, including punctuation
* optionally uses a larger Unicode character set
* optionally uses character subsets (numbers, lowercase, uppercase, punctuation)
* optionally generates passwords containing >= X number of a specified character subset, such as at least one lowercase, uppercase, number, and special character

Password generators that did not fit the bill:

* 1Password - You have to specify how many numbers and special characters you want. You can't have a random number from each character set. This is really poor and degrades the randomness of your passwords

I want to mention [`pwgen`](http://sourceforge.net/projects/pwgen/). That is a really good program. You should use it instead of my stupid program.

Here are all the options:

```sh
> pw -h
usage: pw [-h] [--number NUMBER] [--pronounceable] [--lower]
          [--minimum-lower MINIMUM_LOWER] [--upper]
          [--minimum-upper MINIMUM_UPPER] [--digits]
          [--minimum-digits MINIMUM_DIGITS] [--special]
          [--minimum-special MINIMUM_SPECIAL] [--characters CHARACTERS]
          [--unicode]
          [length]

Generate random passwords

positional arguments:
  length                length of password

optional arguments:
  -h, --help            show this help message and exit
  --number NUMBER, -n NUMBER
                        number of passwords to generate
  --pronounceable, -p   Create human pronounceable passwords
  --lower, -l           Use lower case letters
  --minimum-lower MINIMUM_LOWER, -L MINIMUM_LOWER
                        minimum number of lowercase characters required
  --upper, -u           Use upper case letters
  --minimum-upper MINIMUM_UPPER, -U MINIMUM_UPPER
                        minimum number of upper characters required
  --digits, -d          Use digits
  --minimum-digits MINIMUM_DIGITS, -D MINIMUM_DIGITS
                        minimum number of digit characters required
  --special, -s         Use special characters (punctuation)
  --minimum-special MINIMUM_SPECIAL, -S MINIMUM_SPECIAL
                        minimum number of special characters required
  --characters CHARACTERS, -c CHARACTERS
                        Specify individual characters
  --unicode, -Z         Use a very large unicode character set full of
                        untypable characters
```

## Sample output

The default output is a completeley random, 16 character password

```sh
> pw
A\N[2~BM"(Fq2I;u
```

Make three passwords that meet Active Directory's default requirements

```sh
> pw -L 1 -U 1 -D 1 -S 1 -n 3 8
n@0M|~.d
m+d$0i`B
!:y2#{Mu
```

Make ten passwords with an `INSANE` Unicode character set that are guaranteed to be uncrackable:

```sh
> pw -Z -n 5
ձể𝞎ｎὖῄꙘӲẌⱛСÉ𐐬ὰsḺ
𐐰Vὔֆ𝗯𝚉ѳ𝟉𝞲ӟմ𝛙𝗖Ⲭ𝜇ꝍ
𝐆𝖻𝑗𝑚𝜰γƬӓńȰꚇŷ𝘖𝛲ṏą
ɰъԄᾂⲁẅᴢ𝘑ⲻɏ𝓤Ⲍ𝔵𝗶ŇẶ
ΠăѠẈ𝔢άց𝜋ႿⲔƮⱉ𝕤Պ𝑬ᶊ
```

This seems to meet all of my requirements, and appears to be pretty random. I would like to know how to improve it. I think it is random enough to be secure, but I'd love some confirmation from anyone in the know.
