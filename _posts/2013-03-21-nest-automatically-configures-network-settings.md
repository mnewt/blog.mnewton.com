---
title: nest Automatically Configures Network Settings
date: 2013-03-21 17:00
layout: post
---

In my job as a consultant, I frequently connect my computer to different clients' networks. What's more, each client has their own network parameters, be they proxies, static IP settings, peculiar DNS settings, or the like. If that wasn't enough, I need to stay connected to my office networks.

Now, I don't like repetitive tasks. That is what computers are for. I thought there must be a good software package that can handle all this for me.

For a while I tried to like [ControlPlane](http://www.controlplaneapp.com/). While it's a great effort and I sincerely appreciate the application's best intentions, it is not engineered very well. It frequently hangs because any time a script or other element has its execution blocked because it's waiting on network I/O to respond, UI elements are blocked too.

I decided there must be a simpler way. I searched around a bit and found [LocationChanger](http://tech.inhelsinki.nl/locationchanger/), which is a great little script. The idea is awesome but it's just not sophisticated enough for my needs.

So, I took the idea behind LocationChanger and wrote my own script.

[nest](https://github.com/mnewt/nest) is a command line utility that automatically configures your Mac when the network connection state changes.

## When your Mac does things:

- Connects to a network
- Resumes from sleep
- Disconnects from a network
- Starts up

## nest responds by taking acton:

- Sets the Network Location
- (Re)Connects to a VPN
- (Re)Connects to an SSH tunnel


Make nest go by defining **profiles**:

```bash
PROFILE="Coffee Shop"
LOCATION="Public"
VPN="myvpn"
SSH="ssh -f -N -C -D 1080 user@example.com -L 8080/localhost/8080"
MACADDR="0:11:22:33:44:55"
SSID="AT&T"
```

Please have a look. I think you will find it flexible and fun to use, as long as you think shell scripts are fun.

[https://github.com/mnewt/nest](https://github.com/mnewt/nest)
