# Prepare

```shell
brew install ruby
gem install jekyll bundler
bundle install
```

# Develop
```shell
bundle exec jekyll serve
```

# Deploy
```shell
git add .
git commit
git push
```
